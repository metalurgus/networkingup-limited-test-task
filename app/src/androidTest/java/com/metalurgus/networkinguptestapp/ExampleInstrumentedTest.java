package com.metalurgus.networkinguptestapp;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.text.TextUtils;
import android.util.Log;

import com.metalurgus.networkinguptestapp.model.WeatherResponse;
import com.metalurgus.networkinguptestapp.net.OpenWeatherWebService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import rx.functions.Action1;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static final String[] TEST_CITIES = {"2643743", "5128581"};//London and New York
    private static final String DELIMITER = ",";
    private WeatherResponse weatherResponse;

    @Before
    public void setUp() throws Exception {
        OpenWeatherWebService.service.getWeather(TextUtils.join(DELIMITER, TEST_CITIES)).subscribe(new Action1<WeatherResponse>() {
            @Override
            public void call(WeatherResponse weatherResponse) {
                ExampleInstrumentedTest.this.weatherResponse = weatherResponse;
            }
        });

    }

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.metalurgus.networkinguptestapp", appContext.getPackageName());
    }

    @Test
    public void testLondonNewYork() throws Exception {
        assertNotNull(weatherResponse);
        assertEquals(weatherResponse.getList().size(), TEST_CITIES.length);
        assertEquals(weatherResponse.getList().get(0).getId(), Long.valueOf(TEST_CITIES[0]));

    }
}
