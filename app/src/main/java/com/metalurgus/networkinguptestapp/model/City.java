package com.metalurgus.networkinguptestapp.model;

import java.util.List;

/**
 * Created by User on 26.09.2016.
 */

public class City {
    String name;
    Long id;
    List<Weather> weather;
    Main main;

    public String getName() {
        return name;
    }

    public Long getId() {
        return id;
    }

    public List<Weather> getWeather() {
        return weather;
    }

    public Main getMain() {
        return main;
    }
}
