package com.metalurgus.networkinguptestapp.model;

/**
 * Created by User on 26.09.2016.
 */

public class Main {
    Float temp;
    Float humidity;
    Float pressure;
    Float temp_min;
    Float temp_max;

    public Float getTemp() {
        return temp;
    }

    public Float getHumidity() {
        return humidity;
    }

    public Float getPressure() {
        return pressure;
    }

    public Float getTemp_min() {
        return temp_min;
    }

    public Float getTemp_max() {
        return temp_max;
    }
}
