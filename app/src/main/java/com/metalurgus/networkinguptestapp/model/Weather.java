package com.metalurgus.networkinguptestapp.model;

/**
 * Created by User on 26.09.2016.
 */

public class Weather {
    private Long id;
    private String main;
    private String description;
    private String icon;

    public Long getId() {
        return id;
    }

    public String getMain() {
        return main;
    }

    public String getDescription() {
        return description;
    }

    public String getIcon() {
        return icon;
    }
}
