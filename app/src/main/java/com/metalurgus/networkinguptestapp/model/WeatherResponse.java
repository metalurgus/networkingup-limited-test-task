package com.metalurgus.networkinguptestapp.model;

import java.util.List;

/**
 * Created by User on 26.09.2016.
 */

public class WeatherResponse {
    private Integer cnt;
    private List<City> list;

    public Integer getCnt() {
        return cnt;
    }

    public List<City> getList() {
        return list;
    }
}
