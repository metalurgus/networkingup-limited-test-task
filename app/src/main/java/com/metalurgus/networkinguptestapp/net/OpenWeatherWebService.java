package com.metalurgus.networkinguptestapp.net;

import com.metalurgus.networkinguptestapp.model.WeatherResponse;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by User on 26.09.2016.
 */

public interface OpenWeatherWebService {
    public static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String APPID = "2f21ec9b6000626abf0023e6ca9d0e50";
    public static final String PATH = "./data/2.5/group?APPID=" + APPID + "&units=metric";


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    OpenWeatherWebService service = retrofit.create(OpenWeatherWebService.class);

    @GET(PATH)
    Observable<WeatherResponse> getWeather(@Query("id") String cityIds);

}
