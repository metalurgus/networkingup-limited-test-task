package com.metalurgus.networkinguptestapp.ui;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;

import com.metalurgus.networkinguptestapp.R;
import com.metalurgus.networkinguptestapp.model.WeatherResponse;
import com.metalurgus.networkinguptestapp.net.OpenWeatherWebService;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static final String DELIMITER = ",";
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        String[] cities = getResources().getStringArray(R.array.cities);

        loadWeather(cities);

    }

    private void loadWeather(final String[] cities) {
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        OpenWeatherWebService.service
                .getWeather(TextUtils.join(DELIMITER, cities))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<WeatherResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle(R.string.error_title)
                                .setMessage(R.string.error_message)
                                .setPositiveButton(R.string.retry, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        loadWeather(cities);
                                    }
                                })
                                .setNegativeButton(R.string.cancel, null)
                                .show();
                    }

                    @Override
                    public void onNext(WeatherResponse weatherResponse) {
                        WeatherAdapter weatherAdapter = new WeatherAdapter(weatherResponse);
                        recyclerView.setAdapter(weatherAdapter);
                        findViewById(R.id.loading).setVisibility(View.GONE);
                    }
                });
    }
}
