package com.metalurgus.networkinguptestapp.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.metalurgus.networkinguptestapp.R;
import com.metalurgus.networkinguptestapp.model.City;
import com.metalurgus.networkinguptestapp.model.WeatherResponse;
import com.squareup.picasso.Picasso;

/**
 * Created by Vladislav Matviienko on 26.09.2016.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    private static final String ICON_URL_PLACEHOLDER = "http://openweathermap.org/img/w/%s.png";

    private final WeatherResponse weatherResponse;

    public WeatherAdapter(WeatherResponse weatherResponse) {
        this.weatherResponse = weatherResponse;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_city, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City city = weatherResponse.getList().get(position);

        holder.cityName.setText(city.getName());
        holder.description.setText(city.getWeather().get(0).getDescription());
        Picasso
                .with(holder.cityName.getContext())
                .load(String.format(ICON_URL_PLACEHOLDER, city.getWeather().get(0).getIcon()))
                .placeholder(R.drawable.placeholder)
                .resize(100, 100) //To make it look larger. Original size is 50x50
                .into(holder.weatherIcon);
    }

    @Override
    public int getItemCount() {
        return weatherResponse.getCnt();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView weatherIcon;
        TextView description;
        TextView cityName;

        ViewHolder(View itemView) {
            super(itemView);
            cityName = (TextView) itemView.findViewById(R.id.cityName);
            description = (TextView) itemView.findViewById(R.id.description);
            weatherIcon = (ImageView) itemView.findViewById(R.id.weatherIcon);
        }
    }
}
